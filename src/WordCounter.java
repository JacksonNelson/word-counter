import java.awt.FileDialog;
import java.awt.Frame;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

/**
 * @author Jackson Nelson
 * A class that takes in a text file and counts the number of words in the document, ignoring case and punctuation.
 */
public class WordCounter{

	/**
	 * A function that creates a dictionary containing all of the words and their frequencies from the given text document.
	 * @return dictionary The HashMap created from the text file.
	 * @param filename The file being read
	 */
	public static HashMap createDictionary(String filename)
	{
		// The file being scanned
		File f = new File(filename);
		// Our dictionary to store words and their frequency
		HashMap<String, Integer> dictionary = new HashMap<>();
		// A counter for total words
		int count = 0;
		
		try 
		{
			Scanner scan = new Scanner(f);
			// The word being scanned
			String current = null;
			// While the document isn't empty
			while (scan.hasNext())
			{
				current = scan.next().toLowerCase();
				count++;
				// Ignore anything that isn't a "word"
				current = current.replaceAll("[\\W]", "");
				// If the dictionary contains the word, increment it
				if (dictionary.containsKey(current) && current.length() > 0)
				{
					dictionary.put(current, (dictionary.get(current) + 1));
				}
				// Otherwise, add it to the list
				else
				{
					if (current.length() > 0)
					{
						dictionary.put(current, 1);
					}
				}
			}
			scan.close();
			
			// Create a new ArrayList to sort the keys (words) of the hashmap
			List alphabetical = new ArrayList(dictionary.keySet());
			Collections.sort(alphabetical);
			
			// Prompt the user to choose a file to save to
			String fileEntry = null;
			Frame window = null;
			FileDialog dialog = new FileDialog(window, "Choose a save file location", FileDialog.SAVE);
			dialog.setVisible(true);
			fileEntry = dialog.getFile();
			
			// Write to the chosen file
			BufferedWriter bwriter = new BufferedWriter(new FileWriter(fileEntry));
			bwriter.write("Total words: " + count);
			bwriter.newLine();
			bwriter.write("Unique words: " + dictionary.size());
			bwriter.newLine();
			bwriter.write("Word list, alphabetically then word's frequency:");
			bwriter.newLine();
			for (int i = 0; i < dictionary.size(); i++)
			{
				bwriter.write(alphabetical.get(i) + ", " + dictionary.get(alphabetical.get(i)));
				bwriter.newLine();
			}
			bwriter.close();
			
		}
		catch (FileNotFoundException e) 
		{
			System.out.println("File not found");
		}
		catch (IOException e) 
		{
			System.out.println("Exception");
		}
		
		return dictionary;
			
	}

	/**
	 * Main to call the function
	 * @param args The command line arguments
	 */
	public static void main(String[] args) 
	{	
		//String filename = args[0];
		String filename = "hamlet.txt";
		createDictionary(filename);	
	}

}

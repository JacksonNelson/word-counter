/**
 *
 * @author Jackson Nelson
 * The main driver for the GUI Window
 */
public class TempGui {

    /**
     * Main application driver
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        GuiWindow gw = new GuiWindow();
        gw.setVisible(true);
    }
    
}
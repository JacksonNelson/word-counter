import java.awt.FileDialog;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.HashMap;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author Jackson Nelson
 * A class to create a GUI for interaction with the Word Counter.
 */
public class GuiWindow extends JFrame {
    
	// Create Dictionary button
    JButton dictionaryButton = new JButton( "Create Dictionary");
    HashMap<String, Integer> unsortedDictionary = null;
    
    // Save file button
    JButton fileButton = new JButton("Choose a Save File");
    
    // Text in window
    JLabel text = new JLabel("Count the words from a Text File!");
    
    /**
     * Constructor of GuiWindow object, creates frame with title and two buttons.
     */
    public GuiWindow() {
        this.setTitle("Word Counter");
        this.setBounds( 200, 300, 500, 300);
        this.getContentPane().setLayout(null);
        this.setDefaultCloseOperation( JFrame.EXIT_ON_CLOSE);
        
        this.dictionaryButton.setBounds(20, 80, 200, 60);
        this.getContentPane().add(dictionaryButton);
        this.dictionaryButton.addActionListener( new ButtonListener());
        
        this.fileButton.setBounds(240, 80, 200, 60);
        this.getContentPane().add(fileButton);
        this.fileButton.addActionListener( new FileButtonListener());
        
        this.text.setBounds(140, 20, 200, 60);
        this.getContentPane().add(text);
        

    }
    
    /**
     * Function for pushing the "Create Dictionary" button. Creates a new dictionary of the words in the specified file, unsorted.
     */
    public void createDictionaryButtonPush() 
    {
    	// String to use for file input name/
    	String inputFile = null;
    	
    	// Create a window
		Frame window = null;
		
		// File dialog allows user to choose file
		FileDialog dialog = new FileDialog(window, "Choose Input File", FileDialog.LOAD);
		dialog.setVisible(true);
		
		// Get both directory and file
		inputFile = dialog.getDirectory() + dialog.getFile();
		
		// Populate an unsorted hashmap
		unsortedDictionary = WordCounter.createDictionary(inputFile);
    }
    
    // Handle Create Dictionary button push.
    private class ButtonListener implements ActionListener 
    {    
        @Override
        public void actionPerformed( ActionEvent e) 
        {
            GuiWindow.this.createDictionaryButtonPush();
        }
        
    }
    
    // Handle Choose Save File button push.
    private class FileButtonListener implements ActionListener 
    {
        @Override
        public void actionPerformed( ActionEvent e) 
        {
        	// Sort the elements of the created hashmap, choose save file.
        	WordCounter.sortDictionary(unsortedDictionary);
        }
    }
}



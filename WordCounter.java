import java.awt.FileDialog;
import java.awt.Frame;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

/**
 * @author Jackson Nelson
 * A class that takes in a text file and counts the number of words in the document, ignoring case and punctuation.
 */
public class WordCounter{

	// A counter for total words
	static int count = 0;
	
	/**
	 * A function that creates a dictionary containing all of the words and their frequencies from the given text document.
	 * @return dictionary The HashMap created from the text file.
	 * @param filename The file being read.
	 */
	public static HashMap<String, Integer> createDictionary(String inputFile)
	{
		// The file being scanned
		File scannedFile = new File(inputFile);
		// Our dictionary to store words and their frequency
		HashMap<String, Integer> dictionary = new HashMap<>();
		
		try 
		{
			Scanner scan = new Scanner(scannedFile);
			// The word being scanned
			String current = null;
			// While the document isn't empty
			while (scan.hasNext())
			{
				current = scan.next().toLowerCase();
				count++;
				// Ignore anything that isn't a "word"
				current = current.replaceAll("[\\W]", "");
				current = current.replaceAll("[0-9]+", "");
				// If the dictionary contains the word, increment it
				if (dictionary.containsKey(current) && current.length() > 0)
				{
					dictionary.put(current, (dictionary.get(current) + 1));
				}
				// Otherwise, add it to the list
				else
				{
					if (current.length() > 0)
					{
						dictionary.put(current, 1);
					}
				}
			}
			scan.close();
		}
		// Catch error if file not found or if issue writing to file.
		catch (FileNotFoundException e) 
		{
			System.out.println("File not found");
		}
		catch (IOException i) 
		{
			System.out.println("Exception");
		}
		
		return dictionary;
	}
	
	/**
	 * Sorts the hashmap created from the createDictionary method. Does this by creating a new ArrayList, adding the elements of
	 * the hashmap to the ArrayList, then uses the Collections.sort method to order alphabetically. Finally saves to a user
	 * specified save file.
	 * @param dictionary The hashmap being sorted.
	 */
	public static void sortDictionary(HashMap<String, Integer> dictionary)
	{
		// Create a new ArrayList to sort the keys (words) of the hashmap
		List<String> alphabetical = new ArrayList<String>(dictionary.keySet());
		Collections.sort(alphabetical);
		
		// Prompt the user to choose a file to save to
		String saveFile = null;
		Frame window = null;
		FileDialog dialog = new FileDialog(window, "Choose a save file location", FileDialog.SAVE);
		dialog.setVisible(true);
		saveFile = dialog.getDirectory() + dialog.getFile();
		
		// Try to write to the chosen file.
		try
		{
			BufferedWriter bwriter = new BufferedWriter(new FileWriter(saveFile));
			bwriter.write("Total words: " + count);
			bwriter.newLine();
			bwriter.write("Unique words: " + dictionary.size());
			bwriter.newLine();
			bwriter.write("Word list, alphabetically then word's frequency:");
			bwriter.newLine();
			for (int i = 0; i < dictionary.size(); i++)
			{
				bwriter.write(alphabetical.get(i) + ", " + dictionary.get(alphabetical.get(i)));
				bwriter.newLine();
			}
			bwriter.close();
		}
		// Catch error if file not found or if issue writing to file.
		catch(FileNotFoundException e)
		{
			System.out.println("File Not Found.");
		} 
		catch (IOException io)
		{
			System.out.println("Failed to write to file.");
		}
		
	}
}

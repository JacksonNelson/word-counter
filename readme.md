Counting words from a text document - Jackson Nelson

This software scans through a user provided text document and counts the number of times each word
appears. Punctuation, case, and symbols are ignored. The software then outputs the result
to a text file.

Getting started: This software requires the format to be plain text. Be sure that your document is
not stored as a Word document. The contents of this software include several Java classes, including
one for opening and reading a text file, one to create a GUI, and more.

Instructions: Open the command line and run this program with your desired text file. The software
will scan through this text file and identify each word, store it in a HashMap, then sort it with
an ArrayList. Your output will then be displayed in a new text document of your choosing.

Author: Jackson Nelson
nelsonj6@augsburg.edu